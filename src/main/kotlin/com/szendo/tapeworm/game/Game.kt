package com.szendo.tapeworm.game

import kotlin.random.Random

class Game(
    private val playerCount: Int,
    private val random: Random = Random.Default,
    private val eventHandler: (Event) -> Unit
) {
    private val deck: MutableList<Card>

    private val discard = mutableListOf<Card>()

    private val hands: List<MutableList<Card>>
    private val tiles: MutableMap<Coord, Tile>
    private val openTiles: Set<Coord>
        get() = tiles.values.flatMap { tile -> tile.openDirections.map { tile.coord.move(it) } }.toSet()
    private val events: MutableList<Event> = mutableListOf()

    var currentPlayer = 0
        private set
    private var selectedColor: Color? = null
    private var lastCoord: Coord? = null
    private var lastPlayedCard: Card? = null
    private var lastCutTiles: List<Tile>? = null
    private var queuedAction: Action? = null
    private var swapTarget: Int? = null
    private var queuedRingworms = 0

    var winner: Int? = null
        private set

    private inline val Card.id: String
        get() = imageId
    private inline val List<Card>.ids: List<String>
        get() = map { it.id }

    init {
        check(playerCount in 2..8) { "Invalid player count: $playerCount" }
        val shuffledCards = when (playerCount) {
            in 2..4 -> baseCards
            in 5..8 -> baseCards + baseCards
            else -> error("Invalid player count: $playerCount")
        }.shuffled(random)
        hands = (0 until playerCount).map { playerIndex ->
            shuffledCards.subList(5 * playerIndex, 5 * playerIndex + 5).toMutableList()
        }
        deck = shuffledCards.drop(5 * playerCount).toMutableList()
        val startCoord = Coord(10, 5)
        tiles = mutableMapOf(startCoord to Tile(startCard, startCoord, 0, listOf(null, null, null, null), null))

        startGame()
    }

    fun getPlayerView(playerIndex: Int?, includeHistory: Boolean = true) = PlayerView(
        playerIndex,
        currentPlayer,
        winner,
        hands.map { it.size },
        selectedColor,
        lastCoord,
        lastPlayedCard,
        lastCutTiles?.map { tile ->
            PlayerView.Tile(
                tile.tileCard,
                tile.coord,
                tile.rotation,
                null,
                emptyList())
        },
        queuedAction,
        queuedRingworms,
        deck.size,
        discard.size,
        swapTarget,
        if (playerIndex == currentPlayer) swapTarget?.let { hands[it] } else null,
        playerIndex?.let { hands[it] },
        tiles.values.map { tile ->
            PlayerView.Tile(
                tile.tileCard,
                tile.coord,
                tile.rotation,
                tile.cutColor,
                (0..3).map { tile.parents.find { p -> p.second == it }?.first?.coord })
        },
        openTiles.toList(),
        if (includeHistory) {
            (if (events.size > 25) {
                events.subList(events.size - 25, events.size)
            } else events).map { it.sanitizeFor(playerIndex) }
        } else emptyList(),
    )

    private fun reshuffleDeck() {
        addEvent(Event.ReshuffleDeck)
        deck.addAll(discard.shuffled(random))
        discard.clear()
    }

    private fun startGame() {
        addEvent(Event.StartTurn(currentPlayer))
        // TODO send initial state?
        drawCard(1)
    }

    private fun drawCard(cardCount: Int) = drawCard(currentPlayer, cardCount)

    private fun drawCard(playerIndex: Int, count: Int) {
        if (deck.size < count) {
            reshuffleDeck()
        }

        if (deck.size < count) {
            // TODO alternative win condition?
            return
        }

        val cardsToDraw = deck.subList(0, count)
        val drawnCards = cardsToDraw.toList()
        cardsToDraw.clear()
        addEvent(Event.DrawCards(playerIndex, drawnCards.ids))
        hands[playerIndex].addAll(drawnCards)
    }

    fun playCard(cardIndex: Int, coord: Coord, rotation: Int, parentDirection: Int) {
        check(winner == null) { "Game ended, winner: $winner" }
        check(cardIndex in 0..hands[currentPlayer].lastIndex)
        var card: Card? = null
        try {
            card = hands[currentPlayer].removeAt(cardIndex)
            val action = when (card) {
                is TileCard -> playTileCard(card, coord, rotation, parentDirection)
                is CutCard -> playCutCard(card, coord)
            }

            tiles[coord]?.let { tile ->
                if (tile.parents.size > 1) {
                    queuedRingworms = tile.parents.size - 1
                    addEvent(Event.CompleteRingworm(currentPlayer, queuedRingworms))

                    var max = 1
                    val queuedCoords = mutableListOf(tile.coord)
                    val countedCoords = mutableMapOf<Coord, Int>()
                    while (queuedCoords.isNotEmpty()) {
                        val queuedCoord = queuedCoords.removeAt(0)
                        countedCoords.merge(queuedCoord, 1, Int::plus)
                        val queuedTile = tiles.getValue(queuedCoord)
                        if (queuedTile.parents.size > 1) {
                            max += queuedTile.parents.size - 1
                        }
                        queuedTile.parents.forEach { (tile, _) -> queuedCoords.add(tile.coord) }
                    }
                    countedCoords.filterValues { it < max }.keys.forEach { tiles.getValue(it).cutColor = null }
                }
            }

            if (hands[currentPlayer].size <= 2 * queuedRingworms) {
                if (hands[currentPlayer].isNotEmpty()) {
                    addEvent(Event.DiscardCards(currentPlayer, hands[currentPlayer].ids))
                    discard.addAll(hands[currentPlayer])
                    hands[currentPlayer].clear()
                }
                addEvent(Event.Win(currentPlayer))
                winner = currentPlayer
                return
            }

            queuedAction = action?.also {
                addEvent(Event.TriggerAction(currentPlayer, it))
                when (it) {
                    is DigAction -> {
                        drawCard(it.strength)
                    }
                    is PeekAction -> {
                        drawCard(1)
                    }
                    is HatchAction -> {
                    }
                    is SwapAction -> {
                    }
                }
            }

        } catch (e: IllegalStateException) {
            card?.let { hands[currentPlayer].add(cardIndex, card) }
            throw e
        }
    }

    private fun playTileCard(card: TileCard, coord: Coord, rotation: Int, parentDirection: Int): Action? {
        check(queuedAction == null) { "Invalid move" }
        check(queuedRingworms == 0) { "Invalid move" }

        val parentCoord = coord.move(parentDirection)

        val parentTile = tiles[parentCoord] ?: error("Invalid move")
        val parentColor =
            parentTile.tileCard.edges[(parentDirection + 6 - parentTile.rotation) % 4] ?: error("Invalid move")

        check(lastCoord == null || parentCoord == lastCoord)
        if (selectedColor == null) {
            addEvent(Event.SelectColor(currentPlayer, parentColor))
            selectedColor = parentColor
        } else {
            check(selectedColor == parentColor) { "Invalid move" }
        }

        val neighbors = (0..3).map { tiles[coord.move(it)] }

        val tile = Tile(card, coord, rotation, neighbors, selectedColor) // TODO revert if error happens here
        tiles[coord] = tile

        lastCoord = coord
        lastPlayedCard = card
        lastCutTiles = null

        addEvent(Event.PlayTileCard(currentPlayer, card.id, coord, rotation))

        return card.action
    }

    private fun playCutCard(card: CutCard, coord: Coord): Action? {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction == null) { "Invalid move" }
        check(queuedRingworms == 0) { "Invalid move" }

        val tile = checkNotNull(tiles[coord]) { "Invalid tile: $coord" }
        val cutColor = checkNotNull(tile.cutColor) { "Invalid move" }
        val (parentTile, direction) = tile.parents[0]
        check(card.color == null || card.color == cutColor)
        if (selectedColor == null) {
            addEvent(Event.SelectColor(currentPlayer, cutColor))
            selectedColor = cutColor
        } else {
            check(selectedColor == cutColor) { "Invalid move" }
        }

        discard.add(card)

        parentTile.children.remove(tile)
        parentTile.openDirections.add((direction + 2) % 4)

        val tilesRemoved = mutableListOf<Tile>()
        val coordsHandled = mutableSetOf<Coord>()
        val queuedTilesToRemove = mutableListOf(tile)
        while (queuedTilesToRemove.isNotEmpty()) {
            val tileToRemove = queuedTilesToRemove.removeAt(0)
            if (!coordsHandled.add(tileToRemove.coord)) {
                continue
            }

            tiles.remove(tileToRemove.coord)
            tilesRemoved.add(tileToRemove)
            queuedTilesToRemove.addAll(tileToRemove.children)
        }
        discard.addAll(tilesRemoved.map { it.tileCard }.shuffled(random))

        lastPlayedCard = card
        lastCutTiles = tilesRemoved

        addEvent(Event.PlayCutCard(currentPlayer, card.id, coord, tilesRemoved.map { it.tileCard }.ids))

        return null
    }

    fun discardDigCards(cardIndices: Set<Int>) {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction is DigAction) { "Invalid move" }
        val digAction = queuedAction as DigAction
        check(cardIndices.size == digAction.strength) { "Invalid move" }

        addEvent(Event.DiscardCards(currentPlayer, cardIndices.map { hands[currentPlayer][it].id }))
        discard.addAll(cardIndices.sortedDescending().map { (hands[currentPlayer].removeAt(it)) })

        queuedAction = null
    }

    fun selectHatchTarget(playerIndex: Int) {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction is HatchAction) { "Invalid move" }
        check(playerIndex != currentPlayer) { "Invalid move" }
        val hatchAction = queuedAction as HatchAction

        addEvent(Event.ForceDraw(currentPlayer, playerIndex, hatchAction.strength))
        drawCard(playerIndex, hatchAction.strength)

        queuedAction = null
    }

    fun undrawPeekCard(cardIndex: Int) {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction is PeekAction) { "Invalid move" }

        addEvent(Event.UndrawCard(currentPlayer, hands[currentPlayer][cardIndex].id))
        deck.add(0, hands[currentPlayer].removeAt(cardIndex))

        queuedAction = null
    }

    fun selectSwapTarget(playerIndex: Int) {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction is SwapAction) { "Invalid move" }
        check(swapTarget == null) { "Invalid move" }
        check(playerIndex != currentPlayer) { "Invalid move" }

        addEvent(Event.SelectSwapTarget(currentPlayer, playerIndex, (queuedAction as SwapAction).strength))
        swapTarget = playerIndex
    }

    fun swapCards(ownCardIndices: Set<Int>, targetCardIndices: Set<Int>) {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction is SwapAction) { "Invalid move" }
        check(swapTarget != null) { "Invalid move" }
        check(ownCardIndices.size == targetCardIndices.size)
        check(ownCardIndices.size <= (queuedAction as SwapAction).strength)

        val cardsToGive = ownCardIndices.sortedDescending().map { hands[currentPlayer].removeAt(it) }
        val cardsToTake = targetCardIndices.sortedDescending().map { hands[swapTarget!!].removeAt(it) }
        ownCardIndices.sorted().zip(cardsToTake).forEach { (index, card) -> hands[currentPlayer].add(index, card) }
        targetCardIndices.sorted().zip(cardsToGive).forEach { (index, card) -> hands[swapTarget!!].add(index, card) }

        addEvent(Event.SwapCards(currentPlayer, swapTarget!!, cardsToGive.ids, cardsToTake.ids))

        queuedAction = null
        swapTarget = null
    }

    fun discardRingwormCards(cardIndices: Set<Int>) {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction == null) { "Invalid move" }
        check(queuedRingworms > 0) { "Invalid move" }
        check(cardIndices.size == 2 * queuedRingworms) { "Invalid move" }

        addEvent(Event.DiscardCards(currentPlayer, cardIndices.map { hands[currentPlayer][it] }.ids))
        discard.addAll(cardIndices.sortedDescending().map { (hands[currentPlayer].removeAt(it)) }.shuffled(random))

        queuedRingworms = 0
    }

    fun endTurn() {
        check(winner == null) { "Game ended, winner: $winner" }
        check(queuedAction == null) { "Invalid move" }
        check(queuedRingworms == 0) { "Invalid move" }

        addEvent(Event.EndTurn(currentPlayer))
        val lastPlayer = currentPlayer
        currentPlayer = (currentPlayer + 1) % playerCount
        selectedColor = null
        lastCoord = null
        lastPlayedCard = null
        lastCutTiles = null

        addEvent(Event.StartTurn(currentPlayer))
        drawCard(1)

        if (openTiles.isEmpty()) {
            val cutCards: List<CutCard> = hands[currentPlayer].mapNotNull { it as? CutCard }
            if (cutCards.isEmpty() || cutCards.none {
                    if (it.color == null) {
                        tiles.values.any { tile -> tile.cutColor !== null }
                    } else {
                        tiles.values.any { tile -> tile.cutColor == it.color }
                    }
                }) {
                // alternate win condition
                addEvent(Event.Win(lastPlayer))
                winner = lastPlayer
            }
        }
    }

    private fun addEvent(event: Event) {
        eventHandler(event)
        events.add(event)
    }
}
