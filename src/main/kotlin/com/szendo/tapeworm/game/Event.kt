package com.szendo.tapeworm.game

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
sealed class Event {
    open fun sanitizeFor(viewerIndex: Int?): Event = this

    @JsonTypeName("RESHUFFLE_DECK")
    object ReshuffleDeck : Event() {
        override fun toString() = "Shuffling discard pile back into deck"
    }

    @JsonTypeName("START_TURN")
    class StartTurn(val playerIndex: Int) : Event() {
        override fun toString() = "Player $playerIndex starts turn"
    }

    @JsonTypeName("DRAW_CARDS")
    class DrawCards(val playerIndex: Int, val cards: List<String>) : Event() {
        override fun sanitizeFor(viewerIndex: Int?) =
            if (viewerIndex == playerIndex) this
            else DrawCards(playerIndex, cards.map { "" })

        override fun toString() = "Player $playerIndex draws: " + cards.joinToString(",")
    }

    @JsonTypeName("PLAY_TILE_CARD")
    class PlayTileCard(val playerIndex: Int, val card: String, val coord: Coord, val rotation: Int) : Event() {
        override fun toString() = "Player $playerIndex places $card on $coord (rot: $rotation)"
    }

    @JsonTypeName("PLAY_CUT_CARD")
    class PlayCutCard(val playerIndex: Int, val card: String, val coord: Coord, val cutCards: List<String>) : Event() {
        override fun toString() = "Player $playerIndex cuts ${cutCards.size} card(s) with $card on $coord"
    }

    @JsonTypeName("SELECT_COLOR")
    class SelectColor(val playerIndex: Int, val color: Color) : Event() {
        override fun toString() = "Player $playerIndex select color: $color"
    }

    @JsonTypeName("TRIGGER_ACTION")
    class TriggerAction(val playerIndex: Int, val action: Action) : Event() {
        override fun toString(): String = "Player $playerIndex triggers " + when (action) {
            is DigAction -> "dig ×${action.strength}"
            is HatchAction -> "hatch ×${action.strength}"
            is PeekAction -> "peek"
            is SwapAction -> "swap ×${action.strength}"
        }
    }

    @JsonTypeName("DISCARD_CARDS")
    class DiscardCards(val playerIndex: Int, val cards: List<String>) : Event() {
        override fun toString() = "Player $playerIndex discards: " + cards.joinToString(",")
    }

    @JsonTypeName("FORCE_DRAW")
    class ForceDraw(val playerIndex: Int, val targetPlayerIndex: Int, val count: Int) : Event() {
        override fun toString() = "Player $playerIndex forces $targetPlayerIndex to draw $count"
    }

    @JsonTypeName("UNDRAW_CARD")
    class UndrawCard(val playerIndex: Int, val card: String) : Event() {
        override fun sanitizeFor(viewerIndex: Int?) =
            if (viewerIndex == playerIndex) this
            else UndrawCard(playerIndex, "")

        override fun toString() = "Player $playerIndex places $card on the top of the deck"
    }

    @JsonTypeName("SELECT_SWAP_TARGET")
    class SelectSwapTarget(val playerIndex: Int, val targetPlayerIndex: Int, val count: Int) : Event() {
        override fun toString() = "Player $playerIndex chooses $targetPlayerIndex to swap with (up to $count cards)"
    }

    @JsonTypeName("SWAP_CARDS")
    class SwapCards(
        val playerIndex: Int,
        val targetPlayerIndex: Int,
        val cardsGiven: List<String>,
        val cardsTaken: List<String>
    ) : Event() {
        override fun sanitizeFor(viewerIndex: Int?) = when (viewerIndex) {
            playerIndex, targetPlayerIndex -> this
            else -> SwapCards(playerIndex, targetPlayerIndex, cardsGiven.map { "" }, cardsTaken.map { "" })
        }

        override fun toString() =
            if (cardsGiven.isEmpty()) "Player $playerIndex does not swap any cards with $targetPlayerIndex"
            else "Player $playerIndex swaps with $targetPlayerIndex" +
                    ": gives " + cardsGiven.joinToString(",") +
                    "; takes " + cardsTaken.joinToString(",")
    }

    @JsonTypeName("COMPLETE_RINGWORM")
    class CompleteRingworm(val playerIndex: Int, val ringwormCount: Int) : Event() {
        override fun toString() = "Player $playerIndex completes $ringwormCount ringworm(s)"
    }

    @JsonTypeName("END_TURN")
    class EndTurn(val playerIndex: Int) : Event() {
        override fun toString() = "Player $playerIndex ends turn"
    }

    @JsonTypeName("WIN")
    class Win(val playerIndex: Int) : Event() {
        override fun toString() = "Player $playerIndex wins"
    }
}
