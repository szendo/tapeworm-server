package com.szendo.tapeworm.game

class Tile(
    val tileCard: TileCard,
    val coord: Coord,
    val rotation: Int,
    neighbors: List<Tile?>,
    var cutColor: Color?
) {

    val parents: List<Pair<Tile, Int>>
    val children = mutableSetOf<Tile>()
    val openDirections: MutableSet<Int>

    init {
        val edgesMatch = (0..3).all { direction ->
            val edgeColor = tileCard.edges[(direction + 4 - rotation) % 4]
            neighbors[direction]?.let { neighborTile ->
                edgeColor == neighborTile.tileCard.edges[(direction + 6 - neighborTile.rotation) % 4]
            } ?: true
        }
        check(edgesMatch) { "Invalid tile placement" }

        parents = (0..3).mapNotNull { direction ->
            if (tileCard.edges[(direction + 4 - rotation) % 4] == null) null
            else neighbors[direction]?.let { it to direction }
        }.onEach { (tile, direction) ->
            tile.children.add(this)
            tile.openDirections.remove((direction + 2) % 4)
        }

        openDirections = (0..3).mapNotNull { direction ->
            if (tileCard.edges[(direction + 4 - rotation) % 4] != null && neighbors[direction] == null) direction else null
        }.toMutableSet()
    }
}
