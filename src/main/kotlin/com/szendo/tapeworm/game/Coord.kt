package com.szendo.tapeworm.game

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonFormat(shape = JsonFormat.Shape.ARRAY)
@JsonPropertyOrder("x", "y")
data class Coord constructor(@JsonProperty("x") val x: Int, @JsonProperty("y") val y: Int) {
    operator fun plus(other: Coord) = Coord(x + other.x, y + other.y)
    operator fun minus(other: Coord) = Coord(x - other.x, y - other.y)

    fun move(direction: Int) = this + when (direction) {
        0 -> Coord(0, -1)
        1 -> Coord(1, 0)
        2 -> Coord(0, 1)
        3 -> Coord(-1, 0)
        else -> error("Invalid direction: $direction")
    }

    override fun toString() = "($x;$y)"
}
