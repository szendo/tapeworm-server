package com.szendo.tapeworm.game

enum class Color {
    BLACK, PINK, RED, WHITE
}
