package com.szendo.tapeworm.game

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
sealed class Action

@JsonTypeName("DIG")
class DigAction(val strength: Int) : Action()

@JsonTypeName("HATCH")
class HatchAction(val strength: Int) : Action()

@JsonTypeName("PEEK")
object PeekAction : Action()

@JsonTypeName("SWAP")
class SwapAction(val strength: Int) : Action()
