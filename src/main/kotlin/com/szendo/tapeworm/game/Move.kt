package com.szendo.tapeworm.game

sealed class Move(val card: Card)
data class PlaceMove(val tile: Tile) : Move(tile.tileCard)
data class CutMove(val cutCard: CutCard, val tile: Tile) : Move(cutCard)
