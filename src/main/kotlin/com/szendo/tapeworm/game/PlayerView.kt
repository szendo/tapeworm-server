package com.szendo.tapeworm.game

class PlayerView(
    val thisPlayer: Int?,
    val currentPlayer: Int,
    val winner: Int?,
    val playerCardCounts: List<Int>,
    val selectedColor: Color?,
    val lastCoord: Coord?,
    val lastPlayedCard: Card?,
    val lastCutTiles: List<Tile>?,
    val queuedAction: Action?,
    val queuedRingworms: Int,
    val deckSize: Int,
    val discardSize: Int,
    val targetPlayer: Int?,
    val targetHand: List<Card>?,
    val hand: List<Card>?,
    val tiles: List<Tile>,
    val openTiles: List<Coord>,
    val history: List<Event>,
) {

    class Tile(
        val card: TileCard,
        val coord: Coord,
        val rotation: Int,
        val cutColor: Color?,
        val parents: List<Coord?>
    )
}
