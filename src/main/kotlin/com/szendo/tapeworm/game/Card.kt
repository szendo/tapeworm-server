package com.szendo.tapeworm.game

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
sealed class Card(val imageId: String)

@JsonTypeName("TILE")
data class TileCard(private val _imageId: String, val action: Action?, val edges: List<Color?>) : Card(_imageId)

@JsonTypeName("CUT")
data class CutCard(private val _imageId: String, val color: Color?) : Card(_imageId)
