package com.szendo.tapeworm.io

class RoomInfo(val id: String, val host: Boolean, val members: List<String>)
