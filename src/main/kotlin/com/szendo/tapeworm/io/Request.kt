package com.szendo.tapeworm.io

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(SyncRequest::class, name = "SYNC"),
    JsonSubTypes.Type(SetNameRequest::class, name = "SET_NAME"),
    JsonSubTypes.Type(JoinRoomRequest::class, name = "JOIN_ROOM"),
    JsonSubTypes.Type(LeaveRoomRequest::class, name = "LEAVE_ROOM"),
    JsonSubTypes.Type(StartGameRequest::class, name = "START_GAME"),
    JsonSubTypes.Type(SendGameCommandRequest::class, name = "SEND_GAME_COMMAND"),
)
sealed class Request

@JsonTypeName("SYNC")
object SyncRequest : Request()

@JsonTypeName("SET_NAME")
data class SetNameRequest(@JsonProperty("name") val name: String) : Request()

@JsonTypeName("JOIN_ROOM")
data class JoinRoomRequest(@JsonProperty("roomId") val roomId: String?) : Request()

@JsonTypeName("LEAVE_ROOM")
object LeaveRoomRequest : Request()

@JsonTypeName("START_GAME")
data class StartGameRequest(@JsonProperty("history") val history: Boolean) : Request()

@JsonTypeName("SEND_GAME_COMMAND")
data class SendGameCommandRequest(@JsonProperty("command") val command: GameCommand) : Request()
