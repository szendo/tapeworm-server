package com.szendo.tapeworm.io

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import com.szendo.tapeworm.game.Coord

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(PlayCardCommand::class, name = "PLAY_CARD"),
    JsonSubTypes.Type(DigDiscardCommand::class, name = "DIG_DISCARD"),
    JsonSubTypes.Type(PeekUndrawCommand::class, name = "PEEK_UNDRAW"),
    JsonSubTypes.Type(HatchTargetCommand::class, name = "HATCH_TARGET"),
    JsonSubTypes.Type(SwapTargetCommand::class, name = "SWAP_TARGET"),
    JsonSubTypes.Type(SwapCardsCommand::class, name = "SWAP_CARDS"),
    JsonSubTypes.Type(RingwormDiscardCommand::class, name = "RINGWORM_DISCARD"),
    JsonSubTypes.Type(EndTurnCommand::class, name = "END_TURN"),
)
sealed class GameCommand {
}

@JsonTypeName("PLAY_CARD")
class PlayCardCommand : GameCommand() {
    var cardIndex: Int = 0
    var coord: Coord = Coord(0, 0)
    var rotation: Int = 0
    var direction: Int = 0
}

@JsonTypeName("DIG_DISCARD")
class DigDiscardCommand : GameCommand() {
    var cardIndices: Set<Int> = emptySet()
}

@JsonTypeName("PEEK_UNDRAW")
class PeekUndrawCommand : GameCommand() {
    var cardIndex: Int = 0
}

@JsonTypeName("HATCH_TARGET")
class HatchTargetCommand : GameCommand() {
    var playerIndex: Int = 0
}

@JsonTypeName("SWAP_TARGET")
class SwapTargetCommand : GameCommand() {
    var playerIndex: Int = 0
}

@JsonTypeName("SWAP_CARDS")
class SwapCardsCommand : GameCommand() {
    var ownCardIndices: Set<Int> = emptySet()
    var targetCardIndices: Set<Int> = emptySet()
}

@JsonTypeName("RINGWORM_DISCARD")
class RingwormDiscardCommand : GameCommand() {
    var cardIndices: Set<Int> = emptySet()
}

@JsonTypeName("END_TURN")
object EndTurnCommand : GameCommand()
