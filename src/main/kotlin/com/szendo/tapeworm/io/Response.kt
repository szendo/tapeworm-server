package com.szendo.tapeworm.io

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import com.szendo.tapeworm.game.Event
import com.szendo.tapeworm.game.PlayerView

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
sealed class Response

@JsonTypeName("DATA")
class DataResponse(
    val name: String?,
    val room: RoomInfo?,
    val game: PlayerView?
) : Response()

@JsonTypeName("EVENT")
class EventResponse(val event: Event) : Response()

@JsonTypeName("ERROR")
class ErrorResponse(val message: String) : Response()
