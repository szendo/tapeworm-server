package com.szendo.tapeworm.server

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.kkuegler.HumanReadableIdGenerator
import com.github.kkuegler.PermutationBasedHumanReadableIdGenerator
import com.szendo.tapeworm.game.Event
import com.szendo.tapeworm.io.*
import com.szendo.tapeworm.lobby.Room
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runInterruptible
import java.text.Normalizer
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.collections.set

class GameServer(private val objectMapper: ObjectMapper) {
    private val members: ConcurrentMap<PlayerId, MutableList<WebSocketSession>> = ConcurrentHashMap()
    private val playerNames: ConcurrentMap<PlayerId, String> = ConcurrentHashMap()
    private val rooms: ConcurrentMap<RoomId, Room> = ConcurrentHashMap()
    private val playerRooms: ConcurrentMap<PlayerId, RoomId> = ConcurrentHashMap()

    private val idGenerator: HumanReadableIdGenerator = PermutationBasedHumanReadableIdGenerator()

    suspend fun memberJoined(playerId: PlayerId, socket: WebSocketSession) {
        members.computeIfAbsent(playerId) { CopyOnWriteArrayList() }.add(socket)
        socket.sendResponse(createDataResponse(playerId))
    }

    suspend fun memberLeft(playerId: PlayerId, socket: WebSocketSession) {
        members[playerId]?.remove(socket)
        cleanupPlayer(playerId)
    }

    private suspend fun cleanupPlayer(playerId: PlayerId) {
        if (null == members.compute(playerId) { _, sessions -> sessions?.ifEmpty { null } }) {
            val room = playerRooms[playerId]?.let { rooms[it] }
            if (room == null || leaveRoom(playerId, room)) {
                playerNames.remove(playerId)

            } else if (room.members.all { members[it].isNullOrEmpty() }) {
                room.forceStopGame()
                room.members.forEach {
                    playerRooms.remove(it)
                    cleanupPlayer(it)
                }
                rooms.remove(room.id)
            }
        }
    }

    suspend fun handleRequest(playerId: PlayerId, socket: WebSocketSession, request: Request) {
        when (request) {
            is SyncRequest -> {
                socket.sendResponse(createDataResponse(playerId))
            }
            is SetNameRequest -> {
                playerNames[playerId] = request.name
                sendDataResponse(playerId)
            }
            is JoinRoomRequest -> {
                val existingRoomId = playerRooms[playerId]
                if (existingRoomId != null) {
                    if (rooms.containsKey(existingRoomId)) {
                        return socket.sendError("You are already in a room")
                    } else {
                        playerRooms.remove(playerId)
                    }
                }

                val joinRoomId = RoomId(normalizeRoomName(request.roomId) ?: idGenerator.generate())
                val room = rooms.computeIfAbsent(joinRoomId) { Room(it, playerId, mutableListOf()) }
                if (room.isGameInProgress()) {
                    return socket.sendError("Game is in progress")
                }
                val joinedRoom = synchronized(room.members) {
                    room.members.size < 8 && room.members.add(playerId)
                }

                if (joinedRoom) {
                    playerRooms[playerId] = joinRoomId
                    room.members.forEach { memberId ->
                        sendDataResponse(memberId)
                    }
                } else {
                    socket.sendError("Room is full")
                }
            }
            is LeaveRoomRequest -> {
                playerRooms[playerId]?.also { roomId ->
                    rooms[roomId]?.also { room ->
                        if (!leaveRoom(playerId, room)) {
                            socket.sendError("Game is in progress")
                        }
                    }
                }
            }
            is StartGameRequest -> {
                playerRooms[playerId]?.let { roomId ->
                    rooms[roomId]?.let { room ->
                        if (room.host == playerId) {
                            when {
                                room.members.size < 2 -> socket.sendError("Not enough players")
                                room.members.size > 8 -> socket.sendError("Too many players")
                                room.startGame(request.history) -> {
                                    room.members.forEach { memberId ->
                                        sendDataResponse(memberId)
                                    }
                                }
                                else -> {
                                    val message = "Game is in progress"
                                    socket.sendError(message)
                                }
                            }

                        } else {
                            socket.sendError("You are not the host")
                        }
                    }
                }
            }
            is SendGameCommandRequest -> {
                playerRooms[playerId]?.let { roomId ->
                    rooms[roomId]?.let { room ->
                        if (room.isGameInProgress()) {
                            when {
                                room.sendCommand(playerId, request.command) -> {
                                    room.members.forEach { memberId ->
                                        sendDataResponse(memberId)
                                    }
                                }
                                else -> socket.sendError("It is not your turn")
                            }
                        } else {
                            socket.sendError("Game is not in progress")
                        }
                    }
                }
            }
        }
    }

    private suspend fun leaveRoom(playerId: PlayerId, room: Room) =
        if (room.isGameInProgress()) false
        else {
            if (room.host == playerId) {
                rooms.remove(room.id)
                room.members.forEach { memberId ->
                    playerRooms.remove(memberId)
                    sendDataResponse(memberId)
                }
            } else if (room.members.remove(playerId)) {
                playerRooms.remove(playerId)
                room.members.forEach { memberId ->
                    sendDataResponse(memberId)
                }
            }
            true
        }

    @Suppress("UNUSED_PARAMETER")
    suspend fun handleError(playerId: PlayerId, socket: WebSocketSession, e: IllegalStateException) {
        socket.sendError(e.message ?: "Unexpected error")
    }

    private fun createDataResponse(playerId: PlayerId): DataResponse {
        val name = playerNames[playerId]
        val roomId = playerRooms[playerId]
        val room = roomId?.let { rooms[it] }
        val roomInfo = room?.let {
            RoomInfo(it.id.value, it.host == playerId, it.getPlayers().map { id -> playerNames[id] ?: "[unknown]" })
        }
        val game = room?.getPlayerView(playerId)
        return DataResponse(name, roomInfo, game)
    }

    private suspend fun sendDataResponse(playerId: PlayerId) {
        val sockets = members[playerId]
        if (sockets != null && sockets.isNotEmpty()) {
            val dataResponse = createDataResponse(playerId)
            sockets.forEach { return it.sendResponse(dataResponse) }
        }
    }

    private suspend fun sendEventResponse(playerId: PlayerId, event: Event) {
        val sockets = members[playerId]
        if (sockets != null && sockets.isNotEmpty()) {
            val eventResponse = EventResponse(event)
            sockets.forEach { return it.sendResponse(eventResponse) }
        }
    }

    private suspend fun WebSocketSession.sendError(message: String) {
        sendResponse(ErrorResponse(message))
    }

    private suspend fun WebSocketSession.sendResponse(response: Response): Unit = coroutineScope {
        outgoing.send(Frame.Text(runInterruptible(Dispatchers.IO) { objectMapper.writeValueAsString(response) }))
    }

    private fun normalizeRoomName(name: String?) =
        Normalizer.normalize(name?.trim().orEmpty(), Normalizer.Form.NFD)
            .lowercase()
            .replace(Regex("[\\p{Punct} ]+"), "-")
            .replace(Regex("[^\\p{Graph}]"), "")
            .trim('-').ifEmpty { null }

    fun getDebugInfo() = "Players\n" +
            members.entries.joinToString("\n", "", "\n\n") { (id, sessions) ->
                "${playerNames[id] ?: "[unknown]"}: ${sessions.size} session${if (sessions.size == 1) "" else "s"}"
            } +
            "Rooms\n" +
            rooms.values.joinToString("\n", "", "\n\n") { room ->
                "${room.id.value} by ${playerNames[room.host]}: " +
                        "${room.members.joinToString(",", "[", "]") { "${playerNames[it]}" }}, " +
                        if (room.game == null) "lobby" else "in-game"
            } +
            "Player-rooms\n" +
            playerRooms.entries.joinToString("\n", "", "\n\n") { (pid, rid) ->
                "${playerNames[pid]}: ${rid.value}"
            }
}
