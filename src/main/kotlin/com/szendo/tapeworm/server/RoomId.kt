package com.szendo.tapeworm.server

@JvmInline
value class RoomId(val value: String)
