package com.szendo.tapeworm.server

@JvmInline
value class PlayerId(val value: String)
