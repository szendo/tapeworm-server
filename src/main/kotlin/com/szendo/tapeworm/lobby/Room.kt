package com.szendo.tapeworm.lobby

import com.szendo.tapeworm.game.Event
import com.szendo.tapeworm.game.Game
import com.szendo.tapeworm.io.*
import com.szendo.tapeworm.server.PlayerId
import com.szendo.tapeworm.server.RoomId

class Room(val id: RoomId, val host: PlayerId, val members: MutableList<PlayerId>) {
    var game: Game? = null
        private set
    private var includeHistory = true

    private var playerOrder: List<PlayerId> = emptyList()

    fun startGame(includeHistory: Boolean): Boolean {
        if (isGameInProgress()) {
            return false
        }

        this.includeHistory = includeHistory
        playerOrder = members.shuffled()
        game = Game(members.size) {}
        return true
    }

    fun isGameInProgress() = game?.let { it.winner == null } ?: false

    fun forceStopGame() {
        game = null
        playerOrder = emptyList()
    }

    fun getPlayers() = if (game != null) playerOrder else members

    fun getPlayerView(id: PlayerId) =
        game?.getPlayerView(playerOrder.indexOf(id).let { if (it == -1) null else it }, includeHistory)

    fun sendCommand(id: PlayerId, command: GameCommand): Boolean {
        if (game?.let { playerOrder[it.currentPlayer] == id } != true) {
            return false
        }

        game?.let { game ->
            when (command) {
                is PlayCardCommand -> {
                    game.playCard(command.cardIndex, command.coord, command.rotation, command.direction)
                }
                is DigDiscardCommand -> {
                    game.discardDigCards(command.cardIndices)
                }
                is PeekUndrawCommand -> {
                    game.undrawPeekCard(command.cardIndex)
                }
                is HatchTargetCommand -> {
                    game.selectHatchTarget(command.playerIndex)
                }
                is SwapTargetCommand -> {
                    game.selectSwapTarget(command.playerIndex)
                }
                is SwapCardsCommand -> {
                    game.swapCards(command.ownCardIndices, command.targetCardIndices)
                }
                is RingwormDiscardCommand -> {
                    game.discardRingwormCards(command.cardIndices)
                }
                is EndTurnCommand -> {
                    game.endTurn()
                }
            }
            return true
        }

        return false
    }
}
