package com.szendo.tapeworm

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.szendo.tapeworm.io.*
import com.szendo.tapeworm.server.GameServer
import com.szendo.tapeworm.server.PlayerId
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.sessions.*
import io.ktor.util.*
import io.ktor.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runInterruptible
import java.time.Duration

@Suppress("unused")
fun Application.main() {
    TapewormApplication().apply { main() }
}

class TapewormApplication {

    private val objectMapper = ObjectMapper()
    private val server = GameServer(objectMapper)

    private suspend fun Frame.Text.readRequest(): Request = coroutineScope {
        runInterruptible(Dispatchers.IO) { objectMapper.readValue(readText(), Request::class.java) }
    }

    fun Application.main() {
        install(WebSockets) {
            pingPeriod = Duration.ofSeconds(30)
            timeout = Duration.ofSeconds(15)
        }

        install(Sessions) {
            cookie<PlayerSession>("SESSION") {
                serializer = PlayerSession.Serializer
                cookie.maxAgeInSeconds = 0
                cookie.path = "/ws"
            }
        }

        intercept(ApplicationCallPipeline.Features) {
            if (call.request.path() == "/ws") {
                if (call.sessions.get<PlayerSession>() == null) {
                    call.sessions.set(PlayerSession(generateNonce()))
                }
            }
        }

        routing {
            webSocket("/ws") {
                val session = call.sessions.get<PlayerSession>()

                if (session == null) {
                    close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "No session"))
                    return@webSocket
                }

                val playerId = session.getPlayerId()
                server.memberJoined(playerId, this)

                try {
                    for (frame in incoming) {
                        if (frame is Frame.Text) {
                            try {
                                server.handleRequest(playerId, this, frame.readRequest())
                            } catch (e: JsonProcessingException) {
                                call.application.log.warn("Exception during request processing", e)
                                close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "Invalid message"))
                                return@webSocket

                            } catch (e: IllegalStateException) {
                                call.application.log.warn("Exception during request processing", e)
                                server.handleError(playerId, this, e)

                            } catch (e: Exception) {
                                call.application.log.warn("Exception during request processing", e)
                            }

                        } else {
                            close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "Binary frame"))
                            return@webSocket
                        }
                    }
                } finally {
                    server.memberLeft(playerId, this)
                }
            }

            get("/debug-info") {
                call.respondText(ContentType.Text.Plain, HttpStatusCode.OK) {
                    server.getDebugInfo()
                }
            }
        }
    }
}

data class PlayerSession(internal val id: String) {
    fun getPlayerId() = PlayerId(id)

    object Serializer : SessionSerializer<PlayerSession> {
        override fun deserialize(text: String) = PlayerSession(text)
        override fun serialize(session: PlayerSession): String = session.id
    }
}
