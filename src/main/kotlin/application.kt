import com.szendo.tapeworm.TapewormApplication
import io.ktor.server.cio.*
import io.ktor.server.engine.*

fun main() {
    embeddedServer(CIO, port = 8080) {
        TapewormApplication().apply { main() }
    }.start(wait = true)
}
